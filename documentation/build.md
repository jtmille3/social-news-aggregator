# Build Guide

The student news aggregator is built into a docker container, which can be published and pulled from anywhere.  A gradle 
plugin is used to build the project into a docker container.  

[Docker gradle plugin documentation](https://github.com/Transmode/gradle-docker)

The docker repository it is pushed to is linked to the group name.  The group name can be changed in gradle.properties 
, which is currently setup to use my repo named `jtmille3`.  The docker project name is associated with the gradle plugin 
name, which is currently named `student-news-aggregator`.  It can be changed by modifing settings.gradle.

There is a script that can be run, which will `clean`, `build`, and `buildDocker` the project through gradle tasks.  Finally 
it will push the newly built docker container to the project specified within.

The container is currently hosted at [Docker Hub (jtmille3)](https://hub.docker.com/r/jtmille3/student-news-aggregator/).    