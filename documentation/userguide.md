# User Guide

The student news aggregator is a plugin for Canvas that crowd sources student contributions so that the highest value 
contributions rise to the top and the lowest value contributions sink to bottom.  Students and educators can create,
update, and delete their own contributions.  Any contribution can be voted up or down the web feed.  Contributions are 
grouped by course and assignment.  A contribution in course (X) cannot be seen in course (Y).

### Course Contributions

The course contributions can be found in course homepage in the left hand menu labeled as `Contributions`.  When the 
aggregator contains no contributions it will explicitly ask the student if they would like to create one.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/zero.png "No course contributions")

As contributions are added to the system the web feed will continue to grow.  Each contribution is comprised of a `Title`,
`Link`, `Description`, `Created by`, `Created at`, `Votes`, and an `Assignment tag` if the contribution is related to a specific 
assignment.  Any contribution created by the logged in student can also be updated or deleted.  Student (X) cannot update 
or delete student (Y) contributions.  The title of the post contains a hyperlink to the external contribution.  When the 
hyperlink is clicked the resource will open in a new browser window.

By default the contributions are sorted by the vote count and the creation date.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/course.png "Default course contributions")

It also possible to sort the contributions by the newest to see all the latest contributions added to the web feed by 
clicking on the `New Contributions` link in the banner.  Clicking `Course Contributions` will reset to the default sort
order.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/new.png "New course contributions")

A simple text based search is available in the top right corner of the banner.  Type in your text and press enter to filter 
contributions.  Students can clear the search filter by clicking the x icon within the search box or clear the search 
entry and press enter again.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/search.png "Search contributions")

Students can create contributions by clicking the `Contribute` button in the banner.  This brings up a dialog asks for 
a `Title`, `Link` and `Description`.  Only the `Title` and `Link` are required to post a contribution.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/create.png "Create a contribution")

It is also possible to setup aggregations for assignments within the course.  You can find the assignment aggregations 
within the course homepage by clicking on the `Assignments` link, and then clicking on the aggregator you want to view 
from the list. 

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/assignments.png "Different assignments")

The benefit of having an aggregator for assignments is so that students can view contributions that are targeted specifically for the material they are looking 
for.  Assignment contributions are visible through the course contribution aggregator.  Any contribution published 
through the assignment aggregator will automatically be tagged with the assignment name.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/assignment.png "Default assignment contributions")

It is also important to note that a student's vote only counts once.  If a student upvotes a contribution they can still
change their mind and downvote the same contribution, but a student can't upvote or downvote a contribution more than once 
in sequence.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/warning.png "Vote warning")