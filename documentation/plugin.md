# Plugin Installation Guide

This guide will assist with the installation of the plugin within Canvas.  You must be logged in as an administrator to 
install the student aggregator plugin!  

### Install the course plugin

In Canvas navigate to Admin > Settings > Apps.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/externalapps.png "External apps")

Click on the blue `+App` button to add a new external app.  

* Configuration Type = `Paste XML`
* Name = Anything you want to recognize this plugin
* Consumer Key = `key`
* Consumer Secret = `secret`
* Paste in the following XML:

```
    <?xml version="1.0" encoding="UTF-8"?>
    <cartridge_basiclti_link xmlns="http://www.imsglobal.org/xsd/imslticc_v1p0"
                             xmlns:blti = "http://www.imsglobal.org/xsd/imsbasiclti_v1p0"
                             xmlns:lticm ="http://www.imsglobal.org/xsd/imslticm_v1p0"
                             xmlns:lticp ="http://www.imsglobal.org/xsd/imslticp_v1p0"
                             xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
                             xsi:schemaLocation = "http://www.imsglobal.org/xsd/imslticc_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticc_v1p0.xsd
        http://www.imsglobal.org/xsd/imsbasiclti_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsbasiclti_v1p0.xsd
        http://www.imsglobal.org/xsd/imslticm_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticm_v1p0.xsd
        http://www.imsglobal.org/xsd/imslticp_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticp_v1p0.xsd">
        <blti:launch_url>https://cs6460.jeffmiller.name/aggregator</blti:launch_url>
        <blti:title>Course Contributions</blti:title>
        <blti:description>User driven contributions to the class room.</blti:description>
        <blti:extensions platform="canvas.instructure.com">
            <lticm:property name="privacy_level">public</lticm:property>
            <lticm:options name="course_navigation">
                <lticm:property name="text">Contributions</lticm:property>
                <lticm:property name="label">Contributions</lticm:property>
                <lticm:property name="enabled">true</lticm:property>
                <lticm:property name="display_type">default</lticm:property>
                <lticm:property name="windowTarget">_self</lticm:property>
            </lticm:options>
        </blti:extensions>
    </cartridge_basiclti_link>
```

Note the blti:launch_url element with the value of `https://cs6460.jeffmiller.name/aggregator`.  You'll need to modify 
that URL to match your plugin's host and port.  Click `Submit` to install the application.  

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/addapp.png "Add the canvas plugin")

### Test plugin installation

The plugin will be installed for all courses.  If the plugin installed correctly you can navigate to a course home page 
where you should see a new menu item `Contributions`.  Click that link to bring up the aggregator for that course.

### Install the assignment plugin

As the administrator navigate to a course home page.  Click on the `Assignments` link.  Create a new assignment.  You 
may need to scroll down until you find the section called `Submission Type`.  Select `External Tool`.  Enter the same URL 
that you used before to install the course aggregagator and click the `Find` button.  If you entered the correct address then 
Canvas will recognize your previously installed plugin.  

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/assignmentapp.png "Add the canvas plugin to assignments")

Click on the link of the plugin name.  The URL will populate with the correct value.  Click the blue `Select` button to lock 
in the plugin selection. Finally click `Publish and Save`.

![alt text](https://gitlab.com/jtmille3/social-news-aggregator/raw/master/documentation/images/configureassignmentapp.png "Configure the plugin for an assignment")

### Test assignment plugin installation

You will see your new assignment listed in the Assignments page.  Click on the assignment you just created to view the assignments aggregator.

