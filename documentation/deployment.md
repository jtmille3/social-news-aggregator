# Deployment Guide

The deployment process can range from easy to hard depending on the architecture within which you are trying to deploy 
and run the plugin.  This guide will run through each step that may come up within the deployment process.

### Database

The student news aggregator plugin only runs on Postgres.  If you have your own postgres instance that you want to use 
you can simply modify the yml to allow the plugin to connect to it through the `JAVA_OPTS`.

    -Dspring.datasource.url=jdbc:postgresql://192.168.99.100:5432/canvas
    -Dspring.datasource.username=postgres
    -Dspring.datasource.password=postgres
    
### SSL

Canvas supports plugins through the use of iframes.  Modern browsers will not allow unsecured content to be served 
through iframes unless the connection is secured using SSL.  You can configure student news aggregator to use SSL through 
additional `JAVA_OPTS`.

    -Dserver.port=443
    -Dserver.ssl.key-store=/keystore.jks
    -Dserver.ssl.key-store-password=mypassword
    -Dserver.ssl.enabled=true
    
An alternative approach to this is would be to use a load balancer like Apache or Nginx that can do port forwarding with an 
already signed SSL certificate.  If you follow this approach, which I highly recommend, then make sure that you keep the 
`JAVA_OPTS` to enable forward headers.

    -Dserver.use-forward-headers=true

### Docker

This guide uses [docker](https://www.docker.com/) to install, configure and run the student news aggregator plugin.

To install the development version of `Canvas` along with `Postgres` and `Student News Aggregator` then run:  

> docker-compose -f install/all-docker-compose.yml up -d

If you already have Canvas installed and only need `Postgres` and `Student News Aggregator` then run:

> docker-compose -f install/no-canvas-docker-compose.yml up -d

By default Canvas will run on port 3000 and student news aggregator will run on port 9000.  You can change the defaults by  
modifying the yml file parameters.

### Test the installation

There is a test endpoint to make sure the plugin is accessible and SSL is working by navigating to:

    https://<host>:<port>/
    
If the URL resolves correctly you should see the `Test` in big bold letters.

If you also installed the development version of `Canvas` then you can find login information at:

[lbjay/canvas-docker](https://hub.docker.com/r/lbjay/canvas-docker/)

### Troubleshooting

Each docker container will have it's own set of log files.  You can check each by running:

> docker logs -f sna

> docker logs -f postgres

> docker logs -f canvas