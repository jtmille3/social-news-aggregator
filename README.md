Quick Notes

Builds
    ./docker-build.sh
    
Deploy
    docker-compose up -d
    
Removal
    docker-compose down
    docker rmi $(docker images -aq)
    
URL
    http://cs6460-canvas.jeffmiller.name/
    https://cs6460.jeffmiller.name/