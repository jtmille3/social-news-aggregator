package edu.edtech.canvas.contribution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Service
public class ContributionService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Contribution findById(final Long contributionId) {
        final String SQL = "SELECT * FROM contributions WHERE contribution_id = ?";
        return jdbcTemplate.queryForObject(SQL, new ContributionRowMapper(), contributionId);
    }

    public List<Contribution> findAllByCourseId(
            final Long courseId,
            final Long offset,
            final Long limit
    ) {
        final String SQL = "SELECT * FROM courses_contributions_bridge WHERE course_id = ? ORDER BY votes desc, created_dttm desc OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, offset, limit);
    }

    public List<Contribution> findAllNewByCourseId(
            final Long courseId,
            final Long offset,
            final Long limit
    ) {
        final String SQL = "SELECT * FROM courses_contributions_bridge WHERE course_id = ? ORDER BY created_dttm desc OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, offset, limit);
    }

    public List<Contribution> findAllSearchByCourseId(
            final Long courseId,
            final Long offset,
            final Long limit,
            final String search) {
        final String SQL = "SELECT * FROM courses_contributions_bridge WHERE course_id = ? AND (to_tsvector(title_txt) @@ plainto_tsquery(?) OR to_tsvector(description_txt) @@ plainto_tsquery(?) OR to_tsvector(user_id) @@ plainto_tsquery(?)) OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, search, search, search, offset, limit);
    }

    public Long countAllByCourseId(final Long courseId) {
        final String SQL = "SELECT count(*) FROM courses_contributions_bridge WHERE course_id = ?";
        return jdbcTemplate.queryForObject(SQL, new Object[]{courseId}, Long.class);
    }

    public Long countAllSearchByCourseId(
            final Long courseId,
            final String search
    ) {
        final String SQL = "SELECT count(*) FROM courses_contributions_bridge WHERE course_id = ? AND (to_tsvector(title_txt) @@ plainto_tsquery(?) OR to_tsvector(description_txt) @@ plainto_tsquery(?) OR to_tsvector(user_id) @@ plainto_tsquery(?))";
        return jdbcTemplate.queryForObject(SQL, new Object[]{courseId, search, search, search}, Long.class);
    }

    public List<Contribution> findAllByCourseIdAndAssignmentId(
            final Long courseId,
            final Long assignmentId,
            final Long offset,
            final Long limit
    ) {
        final String SQL = "SELECT * FROM assignments_contributions_bridge WHERE course_id = ? AND assignment_id = ? ORDER BY votes desc, created_dttm desc OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, assignmentId, offset, limit);
    }

    public List<Contribution> findAllNewByCourseIdAndAssignmentId(
            final Long courseId,
            final Long assignmentId,
            final Long offset,
            final Long limit
    ) {
        final String SQL = "SELECT * FROM assignments_contributions_bridge WHERE course_id = ? AND assignment_id = ? ORDER BY created_dttm desc OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, assignmentId, offset, limit);
    }

    public Long countAllByCourseIdAndAssignmentId(final Long courseId, final Long assignmentId) {
        final String SQL = "SELECT count(*) FROM assignments_contributions_bridge WHERE course_id = ? AND assignment_id = ?";
        return jdbcTemplate.queryForObject(SQL, new Object[]{courseId, assignmentId}, Long.class);
    }

    public List<Contribution> findAllSearchByCourseIdAndAssignmentId(
            final Long courseId,
            final Long assignmentId,
            final Long offset,
            final Long limit,
            final String search
    ) {
        final String SQL = "SELECT * FROM courses_contributions_bridge WHERE course_id = ? AND assignment_id = ? AND (to_tsvector(title_txt) @@ plainto_tsquery(?) OR to_tsvector(description_txt) @@ plainto_tsquery(?) OR to_tsvector(user_id) @@ plainto_tsquery(?)) OFFSET ? LIMIT ?";
        return jdbcTemplate.query(SQL, new ContributionRowMapper(), courseId, assignmentId, search, search, search, offset, limit);
    }

    public Long countAllSearchByCourseIdAndAssignmentId(final Long courseId, final Long assignmentId, final String search) {
        final String SQL = "SELECT count(*) FROM courses_contributions_bridge WHERE course_id = ? AND assignment_id = ? AND (to_tsvector(title_txt) @@ plainto_tsquery(?) OR to_tsvector(description_txt) @@ plainto_tsquery(?) OR to_tsvector(user_id) @@ plainto_tsquery(?))";
        return jdbcTemplate.queryForObject(SQL, new Object[]{courseId, assignmentId, search, search, search}, Long.class);
    }

    @Transactional
    public Contribution save(final Contribution contribution) {
        if (contribution.getContributionId() == null) {
            return create(contribution);
        } else {
            return update(contribution);
        }
    }

    @Transactional
    public Contribution create(final Contribution contribution) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL = "INSERT INTO contributions (contribution_id, user_id, link_txt, title_txt, description_txt) " +
                "VALUES (nextval('contributions_sequence'), ?, ?, ?, ?)";

        jdbcTemplate.update(
                connection -> {
                    final PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"contribution_id", "created_dttm"});

                    ps.setString(1, contribution.getUserId());
                    ps.setString(2, contribution.getLink());
                    ps.setString(3, contribution.getTitle());
                    ps.setString(4, contribution.getDescription());

                    return ps;
                }, keyHolder
        );

        keyHolder.getKeyList().forEach(map -> {
            map.forEach((k, v) -> {
                if ("contribution_id".equalsIgnoreCase(k)) {
                    contribution.setContributionId((Long) v);
                } else if ("created_dttm".equalsIgnoreCase(k)) {
                    contribution.setCreated((Date) v);
                }
            });
        });

        return contribution;
    }

    @Transactional
    public Contribution update(final Contribution contribution) {
        final String SQL = "UPDATE contributions SET user_id = ?, link_txt = ?, title_txt = ?, description_txt = ? " +
                "WHERE contribution_id = ?";
        jdbcTemplate.update(SQL,
                contribution.getUserId(),
                contribution.getLink(),
                contribution.getTitle(),
                contribution.getDescription(),
                contribution.getContributionId()
        );

        return findById(contribution.getContributionId());
    }

    private class ContributionRowMapper implements RowMapper<Contribution> {

        @Override
        public Contribution mapRow(final ResultSet rs, final int rowNum) throws SQLException {
            final Contribution contribution = new Contribution();

            contribution.setContributionId(rs.getLong("contribution_id"));
            contribution.setCreated(rs.getTimestamp("created_dttm"));
            contribution.setDescription(rs.getString("description_txt"));
            contribution.setLink(rs.getString("link_txt"));
            contribution.setTitle(rs.getString("title_txt"));
            contribution.setUserId(rs.getString("user_id"));

            try {
                contribution.setCourseId(rs.getLong("course_id"));
            } catch (final Exception e) {
            }

            try {
                contribution.setAssignmentId(rs.getLong("assignment_id"));
            } catch (final Exception e) {
            }

            try {
                contribution.setAssignmentName(rs.getString("assignment_name_txt"));
            } catch (final Exception e) {
            }

            try {
                contribution.setVotes(rs.getLong("votes"));
            } catch (final Exception e) {
            }

            return contribution;
        }
    }
}
