package edu.edtech.canvas.contribution;

import edu.edtech.canvas.PageResult;
import edu.edtech.canvas.assignment.AssignmentContribution;
import edu.edtech.canvas.assignment.AssignmentContributionService;
import edu.edtech.canvas.course.CourseContribution;
import edu.edtech.canvas.course.CourseContributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ContributionController {

    @Autowired
    private ContributionService contributionService;

    @Autowired
    private CourseContributionService courseContributionService;

    @Autowired
    private AssignmentContributionService assignmentContributionService;

    @RequestMapping(value = "/courses/{courseId}/contributions", method = RequestMethod.GET)
    public ResponseEntity<PageResult<Contribution>> getCourseContributions(
            @PathVariable final Long courseId,
            @RequestParam(defaultValue = "0") Long offset,
            @RequestParam(defaultValue = "10") Long limit
    ) {
        final List<Contribution> contributions = contributionService.findAllByCourseId(courseId, offset, limit);
        final Long count = contributionService.countAllByCourseId(courseId);
        return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseId}/contributions/{type}", method = RequestMethod.GET)
    public ResponseEntity<PageResult<Contribution>> getCourseContributions(
            @PathVariable final Long courseId,
            @PathVariable final String type,
            @RequestParam(defaultValue = "0") Long offset,
            @RequestParam(defaultValue = "10") Long limit,
            @RequestParam(defaultValue = "") String search
    ) {
        if (ContributionTypeFactory.isSearch(type)) {
            final List<Contribution> contributions = contributionService.findAllSearchByCourseId(courseId, offset, limit, search);
            final Long count = contributionService.countAllSearchByCourseId(courseId, search);
            return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
        } else {
            final List<Contribution> contributions = contributionService.findAllNewByCourseId(courseId, offset, limit);
            final Long count = contributionService.countAllByCourseId(courseId);
            return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/courses/{courseId}/assignments/{assignmentId}/contributions", method = RequestMethod.GET)
    public ResponseEntity<PageResult<Contribution>> getAssignmentContributions(
            @PathVariable final Long courseId,
            @PathVariable final Long assignmentId,
            @RequestParam(defaultValue = "0") Long offset,
            @RequestParam(defaultValue = "10") Long limit
    ) {
        final List<Contribution> contributions = contributionService.findAllByCourseIdAndAssignmentId(courseId, assignmentId, offset, limit);
        final Long count = contributionService.countAllByCourseIdAndAssignmentId(courseId, assignmentId);
        return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseId}/assignments/{assignmentId}/contributions/{type}", method = RequestMethod.GET)
    public ResponseEntity<PageResult<Contribution>> getAssignmentContributions(
            @PathVariable final Long courseId,
            @PathVariable final Long assignmentId,
            @PathVariable final String type,
            @RequestParam(defaultValue = "0") Long offset,
            @RequestParam(defaultValue = "10") Long limit,
            @RequestParam(defaultValue = "") String search
    ) {
        if (ContributionTypeFactory.isSearch(type)) {
            final List<Contribution> contributions = contributionService.findAllSearchByCourseIdAndAssignmentId(courseId, assignmentId, offset, limit, search);
            final Long count = contributionService.countAllSearchByCourseIdAndAssignmentId(courseId, assignmentId, search);
            return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
        } else {
            final List<Contribution> contributions = contributionService.findAllNewByCourseIdAndAssignmentId(courseId, assignmentId, offset, limit);
            final Long count = contributionService.countAllByCourseIdAndAssignmentId(courseId, assignmentId);
            return new ResponseEntity<>(new PageResult<>(offset, limit, count, contributions), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/contributions", method = RequestMethod.POST)
    public ResponseEntity<Contribution> createCourseContribution(
            @RequestBody final Contribution contribution
    ) {
        final Contribution c = contributionService.save(contribution);

        if (c.getAssignmentId() != null) {
            final AssignmentContribution assignmentContribution = new AssignmentContribution();
            assignmentContribution.setAssignmentId(contribution.getAssignmentId());
            assignmentContribution.setAssignmentName(contribution.getAssignmentName());
            assignmentContribution.setCourseId(contribution.getCourseId());
            assignmentContribution.setContributionId(c.getContributionId());
            assignmentContributionService.save(assignmentContribution);
        } else {
            final CourseContribution courseContribution = new CourseContribution();
            courseContribution.setCourseId(contribution.getCourseId());
            courseContribution.setContributionId(c.getContributionId());
            courseContributionService.save(courseContribution);
        }

        return new ResponseEntity<>(c, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/contributions/{contributionId}", method = RequestMethod.PUT)
    public ResponseEntity<Contribution> updateCourseContribution(
            @PathVariable final Long contributionId,
            @RequestBody final Contribution contribution
    ) {
        return new ResponseEntity<>(contributionService.save(contribution), HttpStatus.OK);
    }

    @RequestMapping(value = "/contributions/{contributionId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCourseContribution(
            @PathVariable final Long contributionId
    ) {
        try {
            courseContributionService.deleteByContributionId(contributionId);
        } catch(final Exception e) {}

        try {
            assignmentContributionService.deleteByContributionId(contributionId);
        } catch(final Exception e) {}

        return new ResponseEntity(HttpStatus.OK);
    }
}
