package edu.edtech.canvas.contribution;

public class ContributionTypeFactory {

    public static boolean isSearch(final String type) {
        return "search".equalsIgnoreCase(type);
    }

    public static boolean isNew(final String type) {
        return "new".equalsIgnoreCase(type);
    }

    public static boolean isDefault(final String type) {
        return !isSearch(type) && !isNew(type);
    }
}
