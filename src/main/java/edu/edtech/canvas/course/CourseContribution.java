package edu.edtech.canvas.course;

public class CourseContribution {

    private Long courseContributionId;

    private Long courseId;

    private Long contributionId;

    public Long getCourseContributionId() {
        return courseContributionId;
    }

    public void setCourseContributionId(Long courseContributionId) {
        this.courseContributionId = courseContributionId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getContributionId() {
        return contributionId;
    }

    public void setContributionId(Long contributionId) {
        this.contributionId = contributionId;
    }
}
