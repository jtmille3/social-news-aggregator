package edu.edtech.canvas.course;

import edu.edtech.canvas.contribution.ContributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class CourseContributionService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ContributionService contributionService;

    @Transactional
    public CourseContribution save(final CourseContribution courseContribution) {
        final String SQL = "INSERT INTO courses_contributions (course_contribution_id, course_id, contribution_id) " +
                "VALUES (nextval('courses_contributions_sequence'), ?, ?)";
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            final PreparedStatement pstmt = connection.prepareStatement(SQL, new String[]{"course_contribution_id"});

            pstmt.setLong(1, courseContribution.getCourseId());
            pstmt.setLong(2, courseContribution.getContributionId());

            return pstmt;
        }, keyHolder);

        courseContribution.setCourseContributionId(keyHolder.getKey().longValue());

        return courseContribution;
    }

    public void deleteByContributionId(final Long contributionId) {
        jdbcTemplate.update("DELETE FROM courses_contributions " +
                        "WHERE contribution_id = ?", contributionId);
    }

    private class CourseContributionMapper implements RowMapper<CourseContribution> {

        @Override
        public CourseContribution mapRow(ResultSet rs, int rowNum) throws SQLException {
            final CourseContribution courseContribution = new CourseContribution();

            courseContribution.setContributionId(rs.getLong("contribution_id"));
            courseContribution.setCourseId(rs.getLong("course_id"));

            return courseContribution;
        }
    }
}
