package edu.edtech.canvas.vote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class VoteService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Transactional
    public Vote save(final Vote vote) {
        if (vote.getVoteId() != null) {
            return create(vote);
        } else {
            return update(vote);
        }
    }

    @Transactional
    public Vote create(final Vote vote) {
        final String SQL = "INSERT INTO votes (vote_id, user_id, contribution_id, vote) " +
                "VALUES (nextval('votes_sequence'), ?, ?, ?)";
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            final PreparedStatement pstmt = connection.prepareStatement(SQL, new String[]{"vote_id"});

            pstmt.setString(1, vote.getUserId());
            pstmt.setLong(2, vote.getContributionId());
            pstmt.setInt(3, vote.getVote());

            return pstmt;
        }, keyHolder);

        vote.setVoteId(keyHolder.getKey().longValue());

        return vote;
    }

    @Transactional
    public Vote update(final Vote vote) {
        final String SQL = "UPDATE votes SET user_id = ?, contribution_id = ?, vote = ? WHERE vote_id = ?";
        jdbcTemplate.update(SQL,
                vote.getUserId(),
                vote.getContributionId(),
                vote.getVote(),
                vote.getVoteId()
        );
        return vote;
    }

    @Transactional
    public void delete(final Long voteId) {
        final String SQL = "DELETE FROM votes WHERE vote_id = ?";
        jdbcTemplate.update(SQL, voteId);
    }

    public Vote findByVoteId(final Long voteId) {
        final String SQL = "SELECT * FROM votes WHERE vote_id = ?";
        return jdbcTemplate.queryForObject(SQL, new VoteRowMapper(), voteId);
    }

    public Vote findByContributionIdAndUserId(final Long contributionId, final String userId) {
        final String SQL = "SELECT * FROM votes WHERE contribution_id = ? AND user_id = ?";
        try {
            return jdbcTemplate.queryForObject(SQL, new VoteRowMapper(), contributionId, userId);
        } catch(final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private class VoteRowMapper implements RowMapper<Vote> {

        @Override
        public Vote mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Vote vote = new Vote();

            vote.setContributionId(rs.getLong("contribution_id"));
            vote.setUserId(rs.getString("user_id"));
            vote.setVoteId(rs.getLong("vote_id"));
            vote.setVote(rs.getInt("vote"));

            return vote;
        }
    }

}