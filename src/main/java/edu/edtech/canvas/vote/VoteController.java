package edu.edtech.canvas.vote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class VoteController {

    @Autowired
    VoteService voteService;

    @RequestMapping(value = "/contributions/{contributionId}/vote", method = RequestMethod.PUT)
    public ResponseEntity upVote(
            final Principal principal,
            @PathVariable final Long contributionId,
            @RequestBody final Vote vote
    ) {
        final Vote history = voteService.findByContributionIdAndUserId(contributionId, principal.getName());

        if (history == null) {
            vote.setUserId(principal.getName());
            return new ResponseEntity(voteService.create(vote), HttpStatus.CREATED);
        } else {
            // Validate the vote.
            if ((history.getVote() == 1 && vote.getVote() == 1) || (history.getVote() == -1 && vote.getVote() == -1)) {
                return new ResponseEntity(history, HttpStatus.CONFLICT);
            }

            history.setVote(vote.getVote());
            return new ResponseEntity(voteService.update(history), HttpStatus.OK);
        }
    }
}
