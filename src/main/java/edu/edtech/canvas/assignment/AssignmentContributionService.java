package edu.edtech.canvas.assignment;

import edu.edtech.canvas.contribution.ContributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class AssignmentContributionService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ContributionService contributionService;

    @Transactional
    public AssignmentContribution save(final AssignmentContribution assignmentContribution) {
        final String SQL = "INSERT INTO assignments_contributions (assignment_contribution_id, assignment_id, course_id, contribution_id, assignment_name_txt) " +
                "VALUES (nextval('assignments_contributions_sequence'), ?, ?, ?, ?)";
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            final PreparedStatement pstmt = connection.prepareStatement(SQL, new String[]{"assignment_contribution_id"});

            pstmt.setLong(1, assignmentContribution.getAssignmentId());
            pstmt.setLong(2, assignmentContribution.getCourseId());
            pstmt.setLong(3, assignmentContribution.getContributionId());
            pstmt.setString(4, assignmentContribution.getAssignmentName());

            return pstmt;
        }, keyHolder);

        assignmentContribution.setAssignmentContributionId(keyHolder.getKey().longValue());

        return assignmentContribution;
    }

    public void deleteByContributionId(final Long contributionId) {
        jdbcTemplate.update("DELETE FROM assignments_contributions " +
                        "WHERE contribution_id = ?", contributionId);
    }

    private class CourseContributionMapper implements RowMapper<AssignmentContribution> {

        @Override
        public AssignmentContribution mapRow(ResultSet rs, int rowNum) throws SQLException {
            final AssignmentContribution assignmentContribution = new AssignmentContribution();

            assignmentContribution.setContributionId(rs.getLong("contribution_id"));
            assignmentContribution.setAssignmentId(rs.getLong("assignment_id"));
            assignmentContribution.setAssignmentName(rs.getString("assignment_name_txt"));
            assignmentContribution.setCourseId(rs.getLong("course_id"));

            return assignmentContribution;
        }
    }
}
