package edu.edtech.canvas.assignment;

public class AssignmentContribution {

    private Long assignmentContributionId;

    private Long assignmentId;

    private String assignmentName;

    private Long courseId;

    private Long contributionId;

    public Long getAssignmentContributionId() {
        return assignmentContributionId;
    }

    public void setAssignmentContributionId(Long assignmentContributionId) {
        this.assignmentContributionId = assignmentContributionId;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getContributionId() {
        return contributionId;
    }

    public void setContributionId(Long contributionId) {
        this.contributionId = contributionId;
    }
}
