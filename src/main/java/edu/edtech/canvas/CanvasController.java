package edu.edtech.canvas;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.imsglobal.aspect.Lti;
import org.imsglobal.lti.launch.LtiSigner;
import org.imsglobal.lti.launch.LtiVerificationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CanvasController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CanvasController.class.getName());

    @Autowired
    LtiSigner signer;

    @Autowired
    ObjectMapper objectMapper;

    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String index() {
        return "test";
    }

    @Lti
    @RequestMapping(value = "/aggregator", method = RequestMethod.POST)
    public String aggregator(
            final HttpServletRequest request,
            final LtiVerificationResult result,
            final HttpServletResponse resp,
            final ModelMap map
    ) throws Throwable {
        if (LOGGER.isDebugEnabled()) {
            debugging(request);
        }

        if (!result.getSuccess()) {
            LOGGER.info("Lti verification failed! error was: " + result.getError());
            LOGGER.info("   message: " + result.getMessage());
            map.put("ltiError", result.getError().toString());
            resp.setStatus(HttpStatus.FORBIDDEN.value());

            return "error";
        } else {
            map.put("name", result.getLtiLaunchResult().getUser().getId());

            final Map<String, String> params = new HashMap<>();

            for (String param : Collections.list(request.getParameterNames())) {
                params.put(param, request.getParameter(param));
            }

            map.put("params", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(params));

            map.put("launch", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result.getLtiLaunchResult()));

            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(map.get("name"), "password", null));

            return "aggregator";
        }
    }

    private void debugging(final HttpServletRequest request) {
        LOGGER.info("Print out headers for trouble shooting.");

        final Enumeration<String> headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            final String name = headerNames.nextElement();
            LOGGER.info(name + "=" + request.getHeader(name));
        }

        LOGGER.info("Print out request parameters for trouble shooting.");

        final Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            final String name = parameterNames.nextElement();
            LOGGER.info(name + "=" + request.getParameter(name));
        }
    }

    /**
     * Example rest call to get the user profile using the never expiring access token.
     *
     * @param params
     * @return
     * @throws IOException
     */
    private String getUserProfile(
            final Map<String, String> params
    ) throws IOException {
        final String userId = params.get("custom_canvas_user_id");

        final HttpClient client = HttpClientBuilder.create().build();

        final HttpGet httpGet = new HttpGet(String.format("http://192.168.99.100:3000/api/v1/users/%s/profile", userId));
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("Authorization", "Bearer canvas-docker");

        final HttpResponse response = client.execute(httpGet);

        final int statusCode = response.getStatusLine().getStatusCode();
        LOGGER.info(String.format("Responded with status code %s", statusCode));

        final String body = IOUtils.toString(response.getEntity().getContent());
        LOGGER.info(body);

        return body;
    }

}
