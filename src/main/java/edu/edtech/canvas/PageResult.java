package edu.edtech.canvas;

import java.util.List;

public class PageResult<T> {

    private Long offset;
    private Long limit;
    private Long count;
    private List<T> items;

    public PageResult(
        final Long offset,
        final Long limit,
        final Long count,
        final List<T> items
    ) {
        this.offset = offset;
        this.limit = limit;
        this.count = count;
        this.items = items;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
