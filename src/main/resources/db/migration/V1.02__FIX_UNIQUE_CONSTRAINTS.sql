ALTER TABLE votes DROP CONSTRAINT votes_contribution_id_key;
ALTER TABLE votes ADD CONSTRAINT votes_contribution_id_key UNIQUE (contribution_id, user_id);
