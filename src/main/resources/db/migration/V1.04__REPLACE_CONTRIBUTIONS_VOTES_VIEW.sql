CREATE OR REPLACE VIEW contributions_bridge AS
  SELECT
    c.*,
    cc.course_id,
    CASE WHEN sum(v.vote) is NULL THEN 0
      ELSE sum(v.vote) END as votes
  FROM
    courses_contributions cc
    LEFT JOIN contributions c on cc.contribution_id = c.contribution_id
    LEFT JOIN votes v on v.contribution_id = c.contribution_id
  GROUP BY c.contribution_id, cc.course_contribution_id
  ORDER BY
    votes desc, created_dttm desc;
