CREATE TABLE contributions(
  contribution_id BIGINT PRIMARY KEY,
  user_id VARCHAR NOT NULL,
  link_txt VARCHAR NOT NULL,
  title_txt VARCHAR NOT NULL,
  description_txt VARCHAR,
  created_dttm TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE votes(
  vote_id BIGINT PRIMARY KEY,
  contribution_id BIGINT UNIQUE NOT NULL,
  user_id VARCHAR NOT NULL
);

CREATE TABLE courses_contributions(
  course_contribution_id BIGINT PRIMARY KEY,
  course_id BIGINT NOT NULL,
  contribution_id BIGINT UNIQUE NOT NULL
);

CREATE SEQUENCE contributions_sequence;
CREATE SEQUENCE votes_sequence;
CREATE SEQUENCE courses_contributions_sequence;

ALTER TABLE votes ADD CONSTRAINT fk_votes FOREIGN KEY (contribution_id) REFERENCES contributions (contribution_id);
ALTER TABLE courses_contributions ADD CONSTRAINT fk_course_contributions FOREIGN KEY (contribution_id) REFERENCES contributions (contribution_id);
