DROP VIEW IF EXISTS contributions_bridge;

CREATE OR REPLACE VIEW courses_contributions_bridge AS
 SELECT
  c.*,
  CASE WHEN cc.course_id IS NULL THEN ac.course_id
    ELSE cc.course_id END AS course_id,
  CASE WHEN sum(v.vote) IS NULL THEN 0
  ELSE sum(v.vote) END AS votes
FROM
  contributions c
  LEFT JOIN courses_contributions cc ON c.contribution_id = cc.contribution_id
  LEFT JOIN assignments_contributions ac ON c.contribution_id = ac.contribution_id
  LEFT JOIN votes v ON v.contribution_id = c.contribution_id
GROUP BY c.contribution_id, cc.course_contribution_id, ac.assignment_contribution_id
ORDER BY
  votes desc, created_dttm desc;


CREATE OR REPLACE VIEW assignments_contributions_bridge AS
  SELECT
    c.*,
    ac.assignment_id,
    ac.course_id,
    CASE WHEN sum(v.vote) is NULL THEN 0
      ELSE sum(v.vote) END as votes
  FROM
    contributions c
    LEFT JOIN assignments_contributions ac ON c.contribution_id = ac.contribution_id
    LEFT JOIN votes v ON v.contribution_id = c.contribution_id
  GROUP BY c.contribution_id, ac.assignment_contribution_id
  ORDER BY
    votes desc, created_dttm desc;
