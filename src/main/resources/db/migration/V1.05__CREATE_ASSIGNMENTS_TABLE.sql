CREATE TABLE assignments_contributions(
  assignment_contribution_id BIGINT PRIMARY KEY,
  course_id BIGINT NOT NULL,
  assignment_id BIGINT NOT NULL,
  contribution_id BIGINT UNIQUE NOT NULL
);

CREATE SEQUENCE assignments_contributions_sequence;

ALTER TABLE assignments_contributions ADD CONSTRAINT fk_assignment_contributions FOREIGN KEY (contribution_id) REFERENCES contributions (contribution_id);