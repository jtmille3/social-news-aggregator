var aggregator = angular.module('ltiApp', []);

aggregator.controller('AggregatorController', ['$http', function ($http) {
    var that = this;

    that.init = function () {
        that.courseId = window.params.custom_canvas_course_id;
        that.assignmentId = window.params.custom_canvas_assignment_id;
        that.assignmentName = window.params.custom_canvas_assignment_title;
        that.userId = window.params.custom_canvas_user_login_id;
        that.contributions = [];
        that.type = '';

        that.loading();

        $('#aggregator-dialog').on('shown.bs.modal', function () {
            $('#titleInput').focus()
        });
    };

    that.$onInit = function () {
        that.fetchContributions();
    };

    that.loading = function () {
        $('.aggregator').hide();
        $('.busy').show();
    };

    that.onSearch = function($event) {
        if($event.keyCode === 13) {
            if (that.search && that.search.length > 0) {
                console.log('onSearch', that.search);
                that.type = '/search';
            } else {
                that.type = '';
            }

            that.fetchContributions();
        }

        if (that.search && that.search.length > 0) {
            $('.aggregator-search-clear').show();
        } else {
            $('.aggregator-search-clear').hide();
        }
    };

    that.onClearSearch = function() {
        that.search = null;
        that.type = '';
        $('.aggregator-search-clear').hide();
        that.fetchContributions();
    }

    that.onChangeType = function(type) {
        that.type = type;
        that.fetchContributions();
    };

    that.fetchContributions = function (offset) {
        offset = offset || 0;

        $('#aggregator-dialog').modal('hide');
        var url = that.assignmentId ?
            '/courses/' + that.courseId + '/assignments/' + that.assignmentId + '/contributions' + that.type :
            '/courses/' + that.courseId + '/contributions' + that.type;
        return $http.get(url + '?offset=' + offset + '&limit=10&search=' + that.search)
            .then(function (result) {
                that.contributions = result.data;
                $('.aggregator').show();
                $('.busy').hide();
            });
    };

    that.onShowContributionDialogWarning = function(vote) {
        that.vote = vote;
        $('#aggregator-dialog-warning').modal({
            backdrop: false,
            keyboard: true,
            show: true
        });
    };

    that.onCloseContributionDialogWarning = function () {
        that.vote = null;
        $('#aggregator-dialog-warning').modal('hide');
    };

    that.onShowContributionDialog = function () {
        $('#aggregator-dialog').modal({
            backdrop: false,
            keyboard: true,
            show: true
        });
    };

    that.onEditContribution = function (contribution) {
        that.contribution = contribution;
        that.onShowContributionDialog();
    };

    that.onCloseContributionDialog = function () {
        that.contribution = null;
        $('#aggregator-dialog').modal('hide');
    };

    that.onPublishContribution = function () {
        if (!that.validateForm(that.contribution)) {
            return;
        }

        that.contribution.userId = that.userId;
        that.contribution.courseId = that.courseId;
        that.contribution.assignmentId = that.assignmentId;
        that.contribution.assignmentName = that.assignmentName;

        that.loading();

        if (!that.contribution.contributionId) {
            $http.post('/contributions', that.contribution).then(function (results) {
                console.log('Success', arguments);
                that.onCloseContributionDialog();
                that.fetchContributions();
            }, function () {
                console.log('Error', arguments);
            });
        } else {
            $http.put('/contributions/' + that.contribution.contributionId, that.contribution).then(function (results) {
                console.log('Success', arguments);
                that.onCloseContributionDialog();
                that.fetchContributions();
            }, function () {
                console.log('Error', arguments);
            });
        }
    };

    that.onUpVote = function (contribution) {
        console.log('Up vote', contribution);
        that.onVote(contribution, 1);
    };

    that.onDownVote = function (contribution) {
        console.log('Down vote', contribution);
        that.onVote(contribution, -1);
    };

    that.onVote = function(contribution, vote) {
        that.loading();
        $http.put('/contributions/' + contribution.contributionId + '/vote', {
            contributionId: contribution.contributionId,
            vote: vote
        }).then(function (results) {
            console.log('Success', arguments);
            that.fetchContributions();
        }, function () {
            console.log('Error', arguments);
            that.fetchContributions();
            that.onShowContributionDialogWarning(vote);
        });
    };

    that.onDeleteContribution = function (contribution) {
        console.log('Delete', contribution);
        that.loading();
        $http.delete('/contributions/' + contribution.contributionId)
            .then(function () {
                that.fetchContributions();
            });
    };

    that.validateForm = function (contribution) {
        var valid = true;

        if (!contribution.title || !contribution.title.length) {
            valid = false;
        }

        if (!contribution.link || !contribution.link.length) {
            valid = false;
        }

        return valid;
    };

    that.isPreviousVisible = function() {
        return that.contributions.offset > 0;
    };

    that.fetchPreviousContributions = function() {
        that.fetchContributions(that.contributions.offset - that.contributions.limit);
    };

    that.isNextVisible = function() {
        return that.contributions.offset + that.contributions.limit < that.contributions.count;
    };

    that.fetchNextContributions = function() {
        that.fetchContributions(that.contributions.offset + that.contributions.limit);
    };

    that.init();
}]);